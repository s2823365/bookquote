package nl.utwente.di.bookQuote;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Quoter {
    double getBookPrice(String isbn){
        Map<String, Double> hashmap = new HashMap<String, Double>();
        ArrayList<String> books = new ArrayList<>();

        books.add("1");
        books.add("2");
        books.add("3");
        books.add("4");
        books.add("5");

        hashmap.put("1", 10.0);
        hashmap.put("2", 45.0);
        hashmap.put("3", 20.0);
        hashmap.put("4", 35.0);
        hashmap.put("5", 50.0);

        if(books.contains(isbn)) {
            return hashmap.get(isbn);
        }else{
            return 0.0;
        }
    }
}
